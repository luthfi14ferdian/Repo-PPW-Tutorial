$(document).ready(function() {
	
	$(".chat-text").bind("keyup", function(send){
	if(send.keyCode == 13){
		var pesan = $("textarea").val();
		$("textarea").val("");
		if(send.keyCode == 13 && send.shiftKey){
			$(".msg-insert").append("<div class=\"msg-send\">" + pesan + "<\div>");
		}
		else if(send.keyCode ==13 && !send.shiftKey){
			$(".msg-insert").append("<div class=\"msg-receive\">" + pesan + "<\div>");

		}
	}
})
	localStorage.themes = '[\
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},\
    {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},\
    {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},\
    {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},\
    {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},\
    {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},\
    {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},\
    {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},\
    {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},\
    {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},\
    {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"} ]';

    if(!localStorage.selectedTheme){
    	localStorage.selectedTheme = '[{"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}}]'
    }
    else{
    	changeColor(localStorage.selectedTheme);
    }

    $(".my-select").select2({
    	'data' : JSON.parse(localStorage.themes)
    })

    $('.apply-button').on('click', function(){  // sesuaikan class button
    console.log($(".my-select").select2("data")[0]);
    console.log(JSON.parse(localStorage.selectedTheme)[0]);
    var bgColor = $(".my-select").select2("data")[0]["bcgColor"];
    var fontColor = $(".my-select").select2("data")[0]["fontColor"];
    var temp = JSON.stringify([{"bcgColor" : bgColor, "fontColor" : fontColor}]);
    localStorage.selectedTheme = temp;
    changeColor(temp);
    })

    function changeColor(data) {
        var obj = JSON.parse(data)[0];
        $(".chat-head").css("background",obj["bcgColor"]);
        $("h2").css('color',obj['fontColor']);

        $(".calculator .model").css("background",obj["bcgColor"]);
        $("h1").css('color',obj['fontColor']);

    }
});



//Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
  	print.value = "";
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value));
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}