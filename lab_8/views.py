from django.shortcuts import render
# Create your views here.
response = {}
def index(request):
    response['author'] = 'Luthfi Ferdian'
    return render(request, "lab_8/lab_8.html", response)